# Generated by Django 3.1.5 on 2021-01-17 15:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0009_auto_20201229_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issue',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(0, 'default'), (1, 'error'), (2, 'csp'), (3, 'transaction')], default=0),
        ),
    ]
